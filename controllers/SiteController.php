<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Clubs;
use app\models\Artistas;
use Mpdf\Mpdf;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        // Obtener los tres primeros artistas de la base de datos
        $artistas = Artistas::find()
                ->limit(3)
                ->all();

        // Obtener los tres clubs con mejor valoración
        $clubs = Clubs::find()
                ->select(['clubs.*', 'AVG(valoraciones.estrellas) AS avg_rating'])
                ->joinWith('valoraciones')
                ->groupBy('clubs.id')
                ->orderBy(['avg_rating' => SORT_DESC])
                ->limit(3)
                ->all();

        return $this->render('index', [
                    'artistas' => $artistas,
                    'clubs' => $clubs,
        ]);
    }

    /**
     * Genera un ticket en PDF para un club específico.
     *
     * @param int $id
     * @return Response
     */
    public function actionDownloadTicket($id) {
        $club = Clubs::findOne($id);
        $ticketNumber = rand(1000000000, 9999999999); // Generar un número de entrada aleatorio
        // Crear PDF
        $mpdf = new Mpdf();

        // Establecer la ruta del archivo CSS
        $cssFile = Yii::getAlias('@web/css/pdf-style.css');

        // Estilo CSS personalizado
        if (file_exists($cssFile)) {
            $style = file_get_contents($cssFile);
            $mpdf->WriteHTML($style, 1);
        } else {
            Yii::error("El archivo CSS $cssFile no se encontró.");
        }

        // Logo en la parte superior central
        $logoPath = Yii::getAlias('@web/img/Logo.png');
        $html = '<div style="text-align: center;"><img src="' . $logoPath . '" style="width: 600px;"></div>';

        // Mensaje de agradecimiento
        $html .= '<div style="text-align: center; font-size: 40px; margin-top: 10px; margin-bottom: 10px;">¡Gracias por descargar nuestra entrada!</div>';

        // Datos del club en una tabla
        $html .= '<table style="margin: auto; margin-top: 20px; margin-bottom: 20px; border-collapse: collapse; width: 80%;">';
        $html .= '<tr><td style="border: 1px solid black; padding: 8px;">Nombre:</td><td style="border: 1px solid black; padding: 8px;">' . $club->nombre . '</td></tr>';
        $html .= '<tr><td style="border: 1px solid black; padding: 8px;">Ubicación:</td><td style="border: 1px solid black; padding: 8px;">' . $club->ubicacion . '</td></tr>';
        $html .= '<tr><td style="border: 1px solid black; padding: 8px;">Horario de Apertura:</td><td style="border: 1px solid black; padding: 8px;">' . $club->h_apertura . '</td></tr>';
        $html .= '<tr><td style="border: 1px solid black; padding: 8px;">Horario de Clausura:</td><td style="border: 1px solid black; padding: 8px;">' . $club->h_clausura . '</td></tr>';
        $html .= '</table>';

        // Número de entrada grande en una sola línea
        $html .= '<div style="text-align: center; font-size: 36px; margin-top: 200px;">Número de entrada: ' . $ticketNumber . '</div>';

        $mpdf->WriteHTML($html);

        // Nombre del archivo PDF
        $filename = 'ticket_' . $club->id . '.pdf';

        // Descargar el PDF
        $mpdf->Output($filename, 'D');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
    if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
        Yii::$app->session->setFlash('contactFormSubmitted');
        return $this->refresh();
    }
    return $this->render('contact', [
        'model' => $model,
    ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
