﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 09/06/2024 14:58:13
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS cnc;

CREATE DATABASE IF NOT EXISTS cnc
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Set default database
--
USE cnc;

--
-- Create table `clubs`
--
CREATE TABLE IF NOT EXISTS clubs (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  ubicacion varchar(200) NOT NULL,
  h_apertura time NOT NULL,
  h_clausura time NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 31,
AVG_ROW_LENGTH = 546,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create table `valoraciones`
--
CREATE TABLE IF NOT EXISTS valoraciones (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_clubs int(11) DEFAULT NULL,
  estrellas decimal(2, 1) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 31,
AVG_ROW_LENGTH = 546,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create index `UK_valoraciones_id_clubs` on table `valoraciones`
--
ALTER TABLE valoraciones
ADD UNIQUE INDEX UK_valoraciones_id_clubs (id_clubs);

--
-- Create foreign key
--
ALTER TABLE valoraciones
ADD CONSTRAINT FK_valoraciones_id_clubs FOREIGN KEY (id_clubs)
REFERENCES clubs (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `servicios`
--
CREATE TABLE IF NOT EXISTS servicios (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  descripcion varchar(200) NOT NULL,
  id_clubs int(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 31,
AVG_ROW_LENGTH = 546,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create foreign key
--
ALTER TABLE servicios
ADD CONSTRAINT FK_servicios_id_clubs FOREIGN KEY (id_clubs)
REFERENCES clubs (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `eventos`
--
CREATE TABLE IF NOT EXISTS eventos (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  descripcion varchar(200) NOT NULL,
  id_clubs int(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 31,
AVG_ROW_LENGTH = 546,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create foreign key
--
ALTER TABLE eventos
ADD CONSTRAINT FK_eventos_id_clubs FOREIGN KEY (id_clubs)
REFERENCES clubs (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `artistas`
--
CREATE TABLE IF NOT EXISTS artistas (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  id_clubs int(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 31,
AVG_ROW_LENGTH = 546,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create foreign key
--
ALTER TABLE artistas
ADD CONSTRAINT FK_artistas_id_clubs FOREIGN KEY (id_clubs)
REFERENCES clubs (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `telefonos`
--
CREATE TABLE IF NOT EXISTS telefonos (
  id int(11) NOT NULL AUTO_INCREMENT,
  telefono varchar(12) DEFAULT NULL,
  id_artistas int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 32,
AVG_ROW_LENGTH = 564,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create index `UK_telefonos` on table `telefonos`
--
ALTER TABLE telefonos
ADD UNIQUE INDEX UK_telefonos (id_artistas, telefono);

--
-- Create foreign key
--
ALTER TABLE telefonos
ADD CONSTRAINT FK_telefonos_id_artistas FOREIGN KEY (id_artistas)
REFERENCES artistas (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table clubs
--
INSERT INTO clubs VALUES
(1, 'Pacific Renewable Energy Corporation', 'Wurzerstraße 1, 93248, Dachsbergau', '00:00:04', '02:38:30'),
(2, 'United Z-Mobile Corp.', 'Prinzregentenstraße 1, 46798, Abenberg', '12:06:43', '20:17:57'),
(3, 'Advanced Space Research Inc.', 'Lissi-Kaeser-Straße 2, 66482, Kuhs', '00:00:08', '01:19:23'),
(4, 'General Petroleum Inc.', 'Dachauer Straße 15d, 98987, Abbau Ader', '12:23:23', '03:08:04'),
(5, 'City Space Explore Group', 'Kanalstraße 28, 07734, Flatow', '08:52:48', '15:26:32'),
(6, 'Global Space Research Inc.', 'Weißenburger Platz 7a, 03911, Biersdorf am See', '07:19:37', '12:36:59'),
(7, 'Pacific High-Technologies Co.', 'Angertorstraße 7, 53730, Aftholderbach', '12:49:25', '13:31:55'),
(8, 'Domestic G-Mobile Inc.', 'Ohmstraße 2, 75926, Großlöbichau', '18:59:12', '21:30:50'),
(9, 'Domestic H-Mobile Group', 'Sammtstraße 1, 34094, Abbau Ader', '10:02:56', '17:37:01'),
(10, 'Creative Fossil Fuel Power Corp.', 'Ackerstraße 25-25, 24176, Altenmedingen', '18:08:10', '05:44:46'),
(11, 'South Travel Corporation', 'Lessingstraße 72c, 01657, Untereisesheim', '00:10:39', '00:00:07'),
(12, 'Union Cars Battery Group', 'Goethestraße 2, 49340, Preußisch Oldendorf', '00:56:40', '13:36:09'),
(13, 'Pacific Space Explore Inc.', 'Bazeillesstraße 2, 35252, Levitzow', '05:09:10', '20:00:27'),
(14, 'United Space Explore Group', 'Ebenauer Straße 1, 72060, Strande', '00:01:06', '11:50:54'),
(15, 'Western Delivery Corporation', 'Luitpoldstraße 6, 34925, Leutersdorf', '00:06:55', '00:38:06'),
(16, 'Flexible Travel Group', 'Sandtnerstraße 1, 19554, Gohlis', '04:05:26', '18:43:16'),
(17, 'Australian Entertainment Co.', 'Pilgersheimer Straße 66e, 75062, Abentheuer', '00:00:25', '04:51:43'),
(18, 'Domestic Wave Power Inc.', 'Franzstraße 19, 43831, Lawalde-Lauba', '01:00:28', '01:00:32'),
(19, 'Domestic O-Mobile Group', 'Bazeillesstraße 1a, 53351, Gaberndorf', '07:22:18', '12:18:46'),
(20, 'Canadian Space Research Co.', 'Dürnbräugasse 2, 21502, Westkinberg', '00:57:34', '21:45:22'),
(21, 'Home N-Mobile Inc.', 'Kaiserstraße 3b, 09047, Döllen', '02:09:09', '04:16:30'),
(22, 'WorldWide Y-Mobile Group', 'Simeonistraße 116a, 34257, Ebrach', '04:16:06', '22:55:34'),
(23, 'North High-Technologies Group', 'Tulbeckstraße 1, 27818, Abenberg', '02:25:46', '14:37:17'),
(24, 'American Space Explore Co.', 'Meindlstraße 26, 57710, Sörgenloch', '13:39:43', '00:14:28'),
(25, 'City Space Research Group', 'Adelheidstraße 2e, 26568, Bechelsdorf', '00:00:10', '00:00:07'),
(26, 'South Space Research Corp.', 'Neufahrner Straße 8c, 39570, Eschenbergen', '06:30:19', '08:07:29'),
(27, 'Domestic Development Co.', 'Vilshofener Straße 14b, 56708, Güsten', '17:18:42', '19:37:36'),
(28, 'Beyond High-Technologies Corporation', 'Aberlestraße 7c, 09423, Marxheim', '00:36:23', '15:30:07'),
(29, 'Canadian High-Technologies Inc.', 'Steinsdorfstraße 2, 31614, Eggingen', '13:06:08', '12:38:15'),
(30, 'East U-Mobile Corporation', 'Beethovenstraße 1, 02858, Ringenwalde', '03:56:29', '00:00:12');

-- 
-- Dumping data for table artistas
--
INSERT INTO artistas VALUES
(1, 'Bertrada', 9),
(2, 'Wolfdietrich', 24),
(3, 'Helvin', 6),
(4, 'Fridurich', 12),
(5, 'Uodalrich', 23),
(6, 'Wastl', 9),
(7, 'Demian', 1),
(8, 'Kunigunde', 10),
(9, 'Wedekind', 6),
(10, 'Bernhardin', 26),
(11, 'Howe', 13),
(12, 'Detlef', 20),
(13, 'Abbeygail ', 29),
(14, 'Bonifaz', 14),
(15, 'Aleida ', 4),
(16, 'Innozenz', 25),
(17, 'Demian', 10),
(18, 'Abby ', 16),
(19, 'Ruvina', 26),
(20, 'Sonea', 6),
(21, 'Miria', 14),
(22, 'Milvina', 29),
(23, 'Amrei', 23),
(24, 'Lexian', 20),
(25, 'Tanaja', 20),
(26, 'Conny', 28),
(27, 'Sonja', 14),
(28, 'Davi', 9),
(29, 'Sonnwin', 23),
(30, 'Wiglev', 27);

-- 
-- Dumping data for table valoraciones
--
INSERT INTO valoraciones VALUES
(1, 30, 0.0),
(2, 8, 1.1),
(3, 29, 0.0),
(4, 2, 0.7),
(5, 21, 2.5),
(6, 24, 2.3),
(7, 6, 4.2),
(8, 18, 0.7),
(9, 12, 0.8),
(10, 9, 0.8),
(11, 13, 1.9),
(12, 28, 5.0),
(13, 25, 1.9),
(14, 10, 0.6),
(15, 15, 4.9),
(16, 3, 4.8),
(17, 19, 5.0),
(18, 16, 3.5),
(19, 22, 5.0),
(20, 7, 1.4),
(21, 14, 4.0),
(22, 11, 4.8),
(23, 26, 3.4),
(24, 23, 4.9),
(25, 27, 5.0),
(26, 20, 4.2),
(27, 17, 2.8),
(28, 4, 2.8),
(29, 1, 5.0),
(30, 5, 2.2);

-- 
-- Dumping data for table telefonos
--
INSERT INTO telefonos VALUES
(21, '614469545', 2),
(7, '794752238', 3),
(9, '783357018', 4),
(19, '799366722', 5),
(23, '778011934', 6),
(8, '654939805', 7),
(12, '652109996', 8),
(18, '624996020', 9),
(5, '625990180', 10),
(10, '650557312', 11),
(17, '631638301', 12),
(3, '664020536', 13),
(6, '665227684', 14),
(27, '639290570', 15),
(30, '646067934', 16),
(4, '638226158', 17),
(25, '688350255', 18),
(28, '600006044', 19),
(2, '776494046', 20),
(20, '797135817', 21),
(29, '778586424', 22),
(15, '668429661', 23),
(22, '601487534', 24),
(26, '752383366', 25),
(14, '786475330', 26),
(16, '642697484', 27),
(24, '702790239', 28),
(1, '763912739', 29),
(13, '603101231', 30);

-- 
-- Dumping data for table servicios
--
INSERT INTO servicios VALUES
(1, 'Retellopor', 'Ut doloremque iste debitis. Voluptate voluptatem explicabo est et totam. Qui dolorum eaque commodi. Ea eligendi tempora magnam quas; perspiciatis ullam quia.', 22),
(2, 'Tabcessollator', 'Aliquid aliquam vero reprehenderit voluptatem inventore. Voluptatem pariatur commodi. Quo nam et ipsa et est et.', 11),
(3, 'Playplottimry', 'Dicta voluptas et non nam quo molestiae. Atque accusamus sit error ab sapiente! Doloribus sit labore ratione.', 7),
(4, 'Rewoofer', 'Eum odit omnis porro perspiciatis ea dolorem. Omnis quas quae quis odio cumque aperiam.', 12),
(5, 'Comcordphone', 'Dicta voluptatem nihil. Eligendi perspiciatis sunt; aperiam facilis atque. Accusantium tempore ut; atque aut quaerat. Natus necessitatibus tenetur. Sed.', 29),
(6, 'Moncorder', 'Veniam consequuntur quis a quia. Aut quo mollitia eos natus fugiat ut. Explicabo sed ut enim voluptates sit omnis.', 10),
(7, 'Conculra', 'Qui optio veritatis. Corporis labore in. Accusantium omnis itaque! Ea est itaque. Officiis est perspiciatis! Recusandae quia quasi. Quas eveniet minus.', 16),
(8, 'Prowoofimator', 'Et aperiam quis error. Et illum ut non qui harum. Quod et unde explicabo quia itaque; in commodi omnis sunt aliquid officiis et.', 8),
(9, 'Cleancessepridge', 'Aut mollitia ut iure enim voluptatibus in...', 7),
(10, 'Anjectadlet', 'Neque impedit perspiciatis autem. Aperiam ut ullam. Ut aut animi et nisi! Quos vero error sed et nemo ab.', 16),
(11, 'Tabpickra', 'Dolorum blanditiis ut. Aliquam dolor quasi! Molestiae aperiam et. Natus voluptatem aut! Est nulla omnis? Et delectus veritatis. Perspiciatis voluptatem in!', 18),
(12, 'Chartiner', 'Esse itaque suscipit. Adipisci qui odio? Perspiciatis non eligendi! Necessitatibus eligendi repudiandae; deserunt sint possimus; neque in qui. Vel molestias!', 16),
(13, 'Subjectra', 'Ipsa sit quia. Repellat aut minima? Molestiae sit sunt! Velit iste sapiente. At et voluptatem. Expedita velit sit. Natus dolore temporibus. Sit iure.', 13),
(14, 'Antellgaer', 'Enim dignissimos harum. Autem est sunt; et est corrupti. Praesentium dolores perspiciatis; praesentium voluptatem rerum. Illum deserunt qui. Voluptatem et.', 10),
(15, 'Tweetcessridge', 'Ut sed voluptas. Molestiae doloribus doloremque? Voluptates laudantium et. Voluptatem odio minima. Qui optio quisquam! Voluptates sed corporis. Perspiciatis.', 23),
(16, 'Readfindonor', 'Laborum cum praesentium est unde temporibus; tempora rem sit molestiae. Numquam illo aut inventore voluptatem rerum quidem!', 7),
(17, 'Anputon', 'Ad enim iste quis voluptas aut quia.', 13),
(18, 'Reputicer', 'Consequatur qui omnis. Qui libero sequi. Quas omnis repellat. Odio tenetur accusamus! Dolorem dolores voluptas; aliquid error quia. Illo eos ut! Veritatis.', 8),
(19, 'Printmutphone', 'Quia commodi laudantium. Non repudiandae odit! Cum vel quisquam. Vitae deserunt autem! Quasi sint corrupti. Natus eligendi culpa; maxime id et. Doloribus.', 22),
(20, 'Supbandfiator', 'Obcaecati est neque; nemo in nobis officiis. Qui velit cumque quasi non vero aliquam! Exercitationem dicta voluptatem eum aperiam quia laboriosam.', 15),
(21, 'Printculanry', 'Similique porro consequuntur. Sequi architecto quasi; nisi et aut; reprehenderit magni quaerat. Voluptatem deserunt aut. Eos eum ullam! Inventore dolorum.', 1),
(22, 'Cleanputlet', 'Qui natus autem minima. Optio dolorum ratione omnis ut est quia. Optio ipsa quia tempora et suscipit ea.', 26),
(23, 'Reniicon', 'Rem ullam iste sapiente saepe voluptatem nobis.', 18),
(24, 'Tableaquer', 'Placeat quia soluta quaerat dicta saepe neque. Est sit quo aut vitae eos! Quos saepe qui libero reprehenderit. Mollitia fugiat rerum!', 2),
(25, 'Transculra', 'Voluptatem incidunt ut alias praesentium aperiam ipsa.', 4),
(26, 'Conlictator', 'Vel nesciunt dicta. Aperiam tempora unde. Quidem qui officiis; et optio itaque. Omnis vitae et? Maiores reprehenderit fugit. Error maxime sint.', 17),
(27, 'Cabbanderor', 'Qui est perferendis. Quia reprehenderit inventore. A aut a. Soluta quia ea. Accusamus consectetur atque. Exercitationem qui quod? Commodi aut enim. Et omnis.', 24),
(28, 'Contoperphone', 'Voluptas rem vero repellat doloremque omnis. Sit autem perspiciatis voluptatibus sint iste doloremque. Itaque nesciunt rerum molestias et. Sit labore qui.', 15),
(29, 'Chartellewator', 'Et quod sequi necessitatibus enim. Omnis repellendus porro rerum odio; unde id laborum aliquid nulla maiores voluptatum. Natus sint sed rerum sit natus!', 22),
(30, 'Comlifilet', 'Ex magnam laborum assumenda mollitia. Error et laboriosam sed consequatur quisquam sed. Doloremque consequatur nemo! Fuga sed velit. Rerum dicta voluptatibus.', 11);

-- 
-- Dumping data for table eventos
--
INSERT INTO eventos VALUES
(1, 'Monofindoller', 'Excepturi sed et enim. Placeat numquam voluptatem sed laborum. Assumenda ratione et sed facilis quis? Aut et facere.', 3),
(2, 'Cleantopefon', 'Libero qui impedit. Repudiandae dolorem natus? Adipisci magnam inventore. Rerum ipsum commodi; obcaecati ullam similique. Eos dolores eveniet. Consequatur.', 3),
(3, 'Transceivscope', 'Temporibus excepturi quas. Expedita qui aut; et voluptas tempora. Nisi molestias neque. Aut magnam officia. Unde aut assumenda. Corrupti sed dignissimos.', 18),
(4, 'Readculor', 'Odio iste placeat dolorem velit esse. Est aliquam ut a? Velit laborum dolor et. Mollitia eaque quos dolorem quia doloribus.', 12),
(5, 'Contopedgor', 'Quis iste pariatur. Magnam qui quia. Inventore nemo sit. Temporibus nemo totam. Culpa soluta consequatur. Et sed sit. Molestiae quibusdam voluptatem; ad.', 20),
(6, 'Readtinaquer', 'Dolorem iste ea. Est impedit ut? Culpa quia ratione. Perspiciatis nihil veniam. Vel et error. Laborum qui fugit. At recusandae ut.', 16),
(7, 'Monlescope', 'Voluptatem voluptate tempora. Omnis quia omnis! Quis quaerat quibusdam. Doloremque natus iste. Possimus iste amet. Repellendus repellat est. Vitae!', 10),
(8, 'Stereolifiridge', 'Deserunt sit et. Officia ut explicabo. Voluptatem eveniet natus! Asperiores minus voluptatem? Tempore natus vitae. Dolores architecto quia; illum nobis?', 28),
(9, 'Cartnierentor', 'Est earum sit ut ullam itaque eligendi.', 29),
(10, 'Micnientor', 'Quidem explicabo quia. Nisi porro deleniti. Quia est esse. Quasi corrupti voluptatem! Tempore ullam voluptatibus. Sed sit quo. Quo voluptatem qui. Distinctio.', 27),
(11, 'Rejectlet', 'Qui molestias commodi vitae dignissimos impedit. Exercitationem atque sunt cupiditate quas reprehenderit et. Voluptatem repellat dolorum. Voluptatem quae qui.', 9),
(12, 'Miclifiedgor', 'Veniam et voluptatum. Hic fugiat corporis voluptates consequatur? Earum dolorem architecto voluptatem error libero. Debitis ut omnis. Nulla natus vero.', 13),
(13, 'Transceiver', 'Quam aliquid amet expedita modi rem veniam.', 17),
(14, 'Readtaerer', 'Maxime eaque totam minima officiis laborum. Unde voluptatem ratione non cumque eligendi! Beatae perferendis consequuntur eius excepturi aut doloremque.', 11),
(15, 'Subwoofgaer', 'Error accusantium blanditiis delectus. Omnis iste optio autem labore. Natus consequatur ratione porro. Quidem quibusdam adipisci!', 10),
(16, 'Conceivphone', 'Enim culpa deleniti sit enim perspiciatis ut. Molestias laboriosam omnis rerum velit qui omnis.', 9),
(17, 'Mictopepor', 'Voluptas tenetur sit ab quis molestias omnis. Unde fugit eum omnis totam non quo.', 12),
(18, 'Montaphone', 'Voluptatem rerum et mollitia dolores commodi aut. Ut nemo id? Laborum voluptatem enim! Reiciendis qui rerum. Animi voluptates iste. Omnis alias...', 4),
(19, 'Armcycllet', 'Et culpa quaerat error quia officia. Ratione ipsum voluptatem ut sit sed! Voluptas non eligendi officia ad ex natus. Necessitatibus excepturi in!', 9),
(20, 'Tweetleefra', 'Et omnis aut; quisquam eligendi cum; aperiam ipsam soluta. Quia temporibus totam? Sed natus ipsa. Neque sint cumque. Deleniti nam voluptatum! Facere est.', 4),
(21, 'Supwoofewry', 'Perferendis nihil aut. Voluptatem autem est consequatur numquam quia natus. Aspernatur consequatur aut voluptas aspernatur vel nihil.', 14),
(22, 'Monjectplridge', 'Expedita aut provident dolorem omnis molestiae. Officia natus iste. Cumque soluta tempore. Blanditiis rerum eum. Error molestiae facilis. Aliquam rem nam.', 10),
(23, 'Micplottor', 'Voluptas reiciendis ullam. Magni voluptatem perspiciatis. Nemo aut molestiae? Aspernatur maiores consequatur. Blanditiis odit vitae. Aut enim est; deleniti.', 13),
(24, 'Comniedgscope', 'Aut voluptatum natus consequatur illo laborum ea.', 12),
(25, 'Speaktaoller', 'Autem eius dolor perspiciatis sit quibusdam quod; et nisi repellendus qui sint ea provident.', 25),
(26, 'Charlictry', 'Placeat nisi vitae. Vel maxime animi impedit voluptatem velit nihil. Libero dolorum exercitationem aliquid aut sed eum. Voluptas ut sit.', 24),
(27, 'Readpickor', 'Unde reprehenderit eum voluptatem nobis sunt dolor.', 12),
(28, 'Transplottentor', 'Quia numquam ipsa saepe vel natus. Consectetur rerum impedit. At fuga aspernatur; voluptas omnis laboriosam. Soluta velit repellendus. Quisquam illo eius.', 20),
(29, 'Amptopplentor', 'A sed ex rerum consectetur eaque blanditiis. Repudiandae ut totam enim! Expedita vitae eius qui. Molestiae dolor libero odit sed perspiciatis.', 14),
(30, 'Transholder', 'Et ipsam sit est sequi qui ipsam. Sit sed accusantium voluptatem sed et corrupti.', 26);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;