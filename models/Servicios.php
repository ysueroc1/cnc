<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicios".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property int $id_clubs
 *
 * @property Clubs $clubs
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicios';
    }

    /**
     * {@inheritdoc}
     */
    
public function rules()
{
    return [
        [['nombre', 'descripcion', 'id_clubs'], 'required'],
        ['nombre', 'match', 'pattern' => '/^(?!.*\s{2,})[a-zA-Z\s]+$/', 'message' => 'El nombre no puede contener doble espacio ni números.'],
        ['nombre', 'string', 'max' => 50],
        ['descripcion', 'string', 'max' => 200],
        ['id_clubs', 'match', 'pattern' => '/^\d{1,10}$/', 'message' => 'El id_clubs solo puede ser un número de hasta 10 dígitos.'],
        ['id_clubs', 'integer'],
        ['id_clubs', 'exist', 'skipOnError' => true, 'targetClass' => Clubs::class, 'targetAttribute' => ['id_clubs' => 'id']],
    ];
}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'id_clubs' => 'Nombre del Club',
        ];
    }

    /**
     * Gets query for [[Clubs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasOne(Clubs::className(), ['id' => 'id_clubs']);
    }
}
