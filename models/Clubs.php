<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clubs".
 *
 * @property int $id
 * @property string $nombre
 * @property string $ubicacion
 * @property string $h_apertura
 * @property string $h_clausura
 *
 * @property Artistas[] $artistas
 * @property Eventos[] $eventos
 * @property Servicios[] $servicios
 * @property Valoraciones $valoraciones
 */
class Clubs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clubs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
{
    return [
        [['nombre', 'ubicacion', 'h_apertura', 'h_clausura'], 'required'],
        [['h_apertura', 'h_clausura'], 'match', 'pattern' => '/^(?:0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', 'message' => 'El formato de hora válido es HH:MM.'],
        [['nombre'], 'match', 'pattern' => '/^[^\s]+(?: [^\s]+)*$/', 'message' => 'El nombre no puede tener doble espacio.'],
        [['ubicacion'], 'match', 'pattern' => '/^[^\s]+(?: [^\s]+)*$/', 'message' => 'La ubicación no puede tener doble espacio ni caracteres especiales.'],
        [['nombre'], 'string', 'max' => 50],
        [['ubicacion'], 'string', 'max' => 200],
    ];
}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ubicacion' => 'Ubicacion',
            'h_apertura' => 'H Apertura',
            'h_clausura' => 'H Clausura',
        ];
    }

    /**
     * Gets query for [[Artistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArtistas()
    {
        return $this->hasMany(Artistas::class, ['id_clubs' => 'id']);
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::class, ['id_clubs' => 'id']);
    }

    /**
     * Gets query for [[Servicios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicios::class, ['id_clubs' => 'id']);
    }

    /**
     * Gets query for [[Valoraciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getValoraciones()
    {
        return $this->hasOne(Valoraciones::class, ['id_clubs' => 'id']);
    }
}
