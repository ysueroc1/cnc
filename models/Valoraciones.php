<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valoraciones".
 *
 * @property int $id
 * @property int|null $id_clubs
 * @property float $estrellas
 *
 * @property Clubs $clubs
 */
class Valoraciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valoraciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
{
    return [
        ['id_clubs', 'match', 'pattern' => '/^\d{1,10}$/', 'message' => 'El id_clubs solo puede contener números y tener como máximo 10 dígitos.'],
        ['estrellas', 'match', 'pattern' => '/^([0-5](\.\d)?)$/', 'message' => 'Las estrellas solo pueden ser números del 0 al 5, con un máximo de una décima.'],
        [['id_clubs'], 'integer'],
        [['estrellas'], 'required'],
        [['estrellas'], 'number'],
        [['id_clubs'], 'unique'],
        [['id_clubs'], 'exist', 'skipOnError' => true, 'targetClass' => Clubs::class, 'targetAttribute' => ['id_clubs' => 'id']],
    ];
}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clubs' => 'Mobre del Club',
            'estrellas' => 'Estrellas',
        ];
    }

    /**
     * Gets query for [[Clubs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasOne(Clubs::className(), ['id' => 'id_clubs']);
    }
}
