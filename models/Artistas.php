<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artistas".
 *
 * @property int $id
 * @property string $nombre
 * @property int $id_clubs
 *
 * @property Clubs $clubs
 * @property Telefonos[] $telefonos
 */
class Artistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artistas';
    }

    /**
     * {@inheritdoc}
     */
    
 public function rules()
{
    return [
        [['nombre', 'id_clubs'], 'required'],
        ['nombre', 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑ]+(?: [a-zA-ZáéíóúÁÉÍÓÚüÜñÑ]+)*$/', 'message' => 'El nombre solo puede contener letras, tildes y espacios, sin números ni caracteres especiales.'],
        ['nombre', 'match', 'pattern' => '/^(?!\s)(?!.*\s{2,}).{1,50}$/', 'message' => 'El nombre no puede tener doble espacio y debe tener como máximo 50 caracteres.'],
        ['id_clubs', 'match', 'pattern' => '/^\d{1,10}$/', 'message' => 'El id_clubs solo puede ser un número de hasta 10 dígitos.'],
        [['id_clubs'], 'integer'],
        [['nombre'], 'string', 'max' => 50],
        [['id_clubs'], 'exist', 'skipOnError' => true, 'targetClass' => Clubs::class, 'targetAttribute' => ['id_clubs' => 'id']],
    ];
}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_clubs' => 'Nombre del Club',
        ];
    }

    /**
     * Gets query for [[Clubs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasOne(Clubs::className(), ['id' => 'id_clubs']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['id_artistas' => 'id']);
    }
}
