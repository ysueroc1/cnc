<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string|null $telefono
 * @property int|null $id_artistas
 *
 * @property Artistas $artistas
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    
    public function rules()
{
     return [
        ['id_artistas', 'match', 'pattern' => '/^\d{1,10}$/', 'message' => 'El id_artistas solo puede contener números y tener como máximo 10 dígitos.'],
        ['telefono', 'match', 'pattern' => '/^\d{9}$/', 'message' => 'El teléfono debe contener exactamente 9 dígitos y solo números.'],
        [['id_artistas'], 'integer'],
        [['telefono'], 'string', 'length' => 9],
        [['id_artistas', 'telefono'], 'unique', 'targetAttribute' => ['id_artistas', 'telefono']],
        [['id_artistas'], 'exist', 'skipOnError' => true, 'targetClass' => Artistas::class, 'targetAttribute' => ['id_artistas' => 'id']],
    ];
}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telefono' => 'Telefono',
            'id_artistas' => 'Nombre del Artista',
        ];
    }

    /**
     * Gets query for [[Artistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArtistas()
    {
        return $this->hasOne(Artistas::className(), ['id' => 'id_artistas']);
    }
}
