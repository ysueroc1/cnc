<?php

use app\models\Clubs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Clubs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cruds">
    <div class="clubs-index">

        <h1><?= Html::encode($this->title) ?></h1>

       <div class="button-container">
            <?= Html::a('Crear Club', ['create'], ['class' => 'btn btn-success']) ?>
       
            <?= Html::a('Descargar PDF', ['clubs/pdf'], ['class' => 'btn btn-danger']) ?>
        </div>


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],


                'nombre',
                'ubicacion',
                'h_apertura',
                'h_clausura',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Clubs $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],
            ],
        ]);
        ?>
    </div>
</div>
