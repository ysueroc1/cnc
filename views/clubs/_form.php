<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Clubs $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clubs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre del club']) ?>

    <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese la ubicacion del club']) ?>

    <?= $form->field($model, 'h_apertura')->textInput(['placeholder' => '00:00-23:59']) ?>

    <?= $form->field($model, 'h_clausura')->textInput(['placeholder' => '00:00-23:59']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
