<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Artistas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="artistas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre del artista']) ?>

    <?= $form->field($model, 'id_clubs')->dropDownList(
    \yii\helpers\ArrayHelper::map(\app\models\Clubs::find()->all(), 'id', 'nombre'),
    ['prompt' => 'Seleccione un club']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
