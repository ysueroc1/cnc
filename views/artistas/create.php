<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Artistas $model */

$this->title = 'Crear Artista';
$this->params['breadcrumbs'][] = ['label' => 'Artistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
