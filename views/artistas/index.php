<?php

use app\models\Artistas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Artistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cruds">
    <div class="artistas-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <div class="button-container">
            <?= Html::a('Crear Artista', ['create'], ['class' => 'btn btn-success']) ?>
       
            <?= Html::a('Descargar PDF', ['artistas/pdf'], ['class' => 'btn btn-danger']) ?>
        </div>


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],


                'nombre',
                [
                    'attribute' => 'id_clubs',
                    'label' => 'Club',
                    'value' => function ($model) {
                        return $model->clubs->nombre;
                    },
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
