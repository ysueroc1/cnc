<?php

use yii\helpers\Html;

$this->title = 'Lista de Artistas';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= Html::encode($this->title) ?></title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 12px;
            color: #333;
            text-align: center;
        }
        .container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
        }
        h2 {
            text-align: center;
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 0 auto;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:nth-child(even) {
            background-color: #f9f9f9;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2><?= Html::encode($this->title) ?></h2>
        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Club</th>
            </tr>
            <?php foreach ($dataProvider->getModels() as $model): ?>
                <tr>
                    <td><?= Html::encode($model->id) ?></td>
                    <td><?= Html::encode($model->nombre) ?></td>
                    <td><?= Html::encode($model->clubs->nombre) ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</body>
</html>
