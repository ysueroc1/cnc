<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Artistas;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

// Obtener el nombre del artista
$artista = Artistas::findOne($model->id_artistas);
$artistaNombre = $artista ? $artista->nombre : 'No asignado';

$this->title = 'Actualizar Telefono: ' . $artistaNombre;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
