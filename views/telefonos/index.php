<?php

use app\models\Telefonos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Telefonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cruds">
    <div class="telefonos-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <div class="button-container">
            <?= Html::a('Crear Telefono', ['create'], ['class' => 'btn btn-success']) ?>
       
             <?= Html::a('Descargar PDF', ['telefonos/pdf'], ['class' => 'btn btn-danger']) ?>
        </div>


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],


                'telefono',
                [
                    'attribute' => 'id_artistas',
                    'label' => 'Artista',
                    'value' => function ($model) {
                        return $model->artistas->nombre;
                    },
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
