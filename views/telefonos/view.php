<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Artistas;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

// Obtener el nombre del artista
$artista = Artistas::findOne($model->id_artistas);
$artistaNombre = $artista ? $artista->nombre : 'No asignado';

$this->title = $artistaNombre;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="telefonos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres borrarlo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'telefono',
            [
                'attribute' => 'id_artistas',
                'label' => 'Artista',
                'value' => $artistaNombre,
            ],
        ],
    ]) ?>

</div>
