<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="telefonos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => '666333999']) ?>

    <?= $form->field($model, 'id_artistas')->dropDownList(
    \yii\helpers\ArrayHelper::map(\app\models\Artistas::find()->all(), 'id', 'nombre'),
    ['prompt' => 'Seleccione un Artista']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
