<?php

use yii\helpers\Html;

$this->title = 'Lista de Eventos';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= Html::encode($this->title) ?></title>
    <style>
        .container {
            width: 100%;
            text-align: center;
            margin-top: 20px;
        }
        h2 {
            font-size: 24px;
            margin-bottom: 20px;
        }
        table {
            width: 80%;
            margin: 0 auto;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
            font-weight: bold;
        }
    </style>
</head>
<body>

<div class="container">
    <h2><?= Html::encode($this->title) ?></h2>

    <table id="eventos">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripciones</th>
            <th>Clubs</th>
        </tr>

        <?php foreach ($dataProvider->getModels() as $model): ?>
            <tr>
                <td><?= Html::encode($model->id) ?></td>
                <td><?= Html::encode($model->nombre) ?></td>
                <td><?= Html::encode($model->descripcion) ?></td>
                <td><?= Html::encode($model->clubs->nombre) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

</body>
</html>
