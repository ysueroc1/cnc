<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Home';
$this->registerCssFile('@web/css/home.css');
?>

<div class="flex flex-col min-h-[100dvh] bg-black">
    <!-- Sección de Logo -->
    <section class="flex items-center justify-center w-full h-20">
        <?= Html::img('@web/img/logo.png', ['class' => 'h-12']) ?>
    </section>

    <!-- Sección de Artistas -->
    <!-- Sección de Artistas -->
    <section class="py-12 sm:py-20 dark:bg-gray-800">
        <div class="container px-4 md:px-6">
            <div class="mb-8 text-center">
                <h2 class="text-3xl sm:text-4xl font-bold mb-2">Artistas Destacados</h2>
                <p class="text-gray-500 dark:text-gray-400">Conoce a algunos artistas en nuestros clubs</p>
            </div>
            <?php
                $imagenes_artistas = [
                '@web/img/artista1.jpg', // Ruta de la imagen para el primer artista
                '@web/img/artista2.jpg', // Ruta de la imagen para el segundo artista
                '@web/img/artista3.jpg', // Ruta de la imagen para el tercer artista
            ];
             ?>
            
            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
                <?php foreach ($artistas as $index => $artista): ?>
                    <div class="rounded-lg border bg-card text-card-foreground shadow-sm" data-v0-t="card">
                        <?= Html::img($imagenes_artistas[$index], ['width' => 600, 'height' => 400, 'alt' => Html::encode($artista['nombre']), 'class' => 'rounded-t-lg object-cover w-full h-48', 'style' => 'aspect-ratio: 600 / 400; object-fit: cover;']) ?>
                        <div class="p-6">
                            <h3 class="text-xl font-bold mb-2"><?= Html::encode($artista['nombre']) ?></h3>
                            <!-- Agregar otros detalles del artista aquí -->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="relative w-full h-[80vh] overflow-hidden clubs-section bg-black"> <!-- Añadir la clase clubs-section -->
        <div class="container px-4 md:px-6">
            <div class="mb-8 text-center">
                <h2 class="text-3xl sm:text-4xl font-bold mb-2">Clubes Mejor Valorados</h2>
                <p class="text-gray-500 dark:text-gray-400">Descubre los Clubs mejor valorados de CNC</p>
            </div>
            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
                <?php
                // Definimos las imágenes para cada club manualmente
                $clubImages = [
                    1 => '@web/img/club1.jpg', // Club 1
                    2 => '@web/img/club2.jpg', // Club 2
                    3 => '@web/img/club3.jpg', // Club 3
                ];

                foreach ($clubs as $index => $club):
                    // Asignamos una imagen a cada club en función del índice
                    $image = isset($clubImages[$index + 1]) ? $clubImages[$index + 1] : '@web/img/default.jpg';
                    ?>
                    <div class="rounded-lg border bg-card text-card-foreground shadow-sm" data-v0-t="card">
                        <?=
                        Html::img($image, [
                            'width' => 600,
                            'height' => 400,
                            'alt' => Html::encode($club->nombre),
                            'class' => 'rounded-t-lg object-cover w-full h-48',
                            'style' => 'aspect-ratio: 600 / 400; object-fit: cover;'
                        ])
                        ?>
                        <div class="p-6">
                            <h3 class="text-xl font-bold mb-2"><?= Html::encode($club->nombre) ?></h3>
                            <p class="text-gray-500 dark:text-gray-400 mb-4"><?= Html::encode($club->ubicacion) ?></p>
                            <div class="flex items-center justify-between">
                                <a href="<?= Url::to(['site/download-ticket', 'id' => $club->id]) ?>" class="inline-flex items-center justify-center rounded-md bg-[#ff6b6b] px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-[#ff4d4d] focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-[#ff6b6b] focus-visible:ring-offset-2">Descargar Entrada</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</div>
