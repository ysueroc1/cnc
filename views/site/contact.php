<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Contact';
$this->registerCssFile('@web/css/contact.css');
?>
<div class="container">
    <main>
        <section class="intro" style="background-image: url('<?= Yii::getAlias('@web/img/Fondo.contacto.jpg') ?>');">
            <div class="intro-content">
                <div class="intro-text">
                    <div class="header-content">
                        <?= Html::img('@web/img/Logo.png', ['alt' => 'Logo', 'class' => 'logo']); ?>
                        <p>¿Necesitas asistencia o tienes alguna pregunta? ¡Contáctanos! Estamos aquí para ayudarte.</p>
                    </div>
                </div>
                <div class="contact-form">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'subject') ?>
                    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
                    
                    <div class="form-group">
                        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>
                    
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </section>
    </main>
</div>
