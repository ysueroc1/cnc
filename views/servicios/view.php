<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Clubs;

/** @var yii\web\View $this */
/** @var app\models\Servicios $model */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="servicios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro de que quierer borrarlo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'descripcion',
            [
                'attribute' => 'id_clubs',
                'label' => 'Club',
                'value' => function($model) {
                    return Clubs::findOne($model->id_clubs)->nombre;
                },
            ],
        ],
    ]) ?>

</div>
