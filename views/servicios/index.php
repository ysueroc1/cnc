<?php

use app\models\Servicios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cruds">
    <div class="servicios-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <div class="button-container">
            <?= Html::a('Crear Servicio', ['create'], ['class' => 'btn btn-success']) ?>
       
             <?= Html::a('Descargar PDF', ['servicios/pdf'], ['class' => 'btn btn-danger']) ?>
        </div>


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],


                'nombre',
                'descripcion',
                [
                    'attribute' => 'id_clubs',
                    'label' => 'Club',
                    'value' => function ($model) {
                        return $model->clubs->nombre;
                    },
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
