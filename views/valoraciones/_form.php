<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Valoraciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="valoraciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_clubs')->dropDownList(
    \yii\helpers\ArrayHelper::map(\app\models\Clubs::find()->all(), 'id', 'nombre'),
    ['prompt' => 'Seleccione un club']) ?>

    <?= $form->field($model, 'estrellas')->textInput(['maxlength' => true, 'placeholder' => '0.0-5.0']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
